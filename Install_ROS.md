# ROS Installation

This page is based on the official [ROS installation page](http://wiki.ros.org/melodic/Installation/Ubuntu).

However, in the WSL Ubuntu 18.04, there are some [GPG bugs](https://github.com/Microsoft/WSL/issues/3286) that are difficult for beginners to solve. **Therefore, please follow this guide.**

1. Open your WSL.
2. Set up your keys. You can copy and paste the following commands. **`Ctrl` + `v` does *NOT* work on Linux terminals. Please use the following method.**
   - If you are using WSL, `right click` on the mouse.
   - If you are using the original Linux, `Ctrl`+`Shift`+`v`

    ```bash
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    wget http://packages.ros.org/ros.key -O - | sudo apt-key add -
    ```

    ![Set up your keys](img/install_ros1.PNG)

3. Update your [Debian](https://en.wikipedia.org/wiki/Debian) package index.
    ```bash
    sudo apt update
    ```

4. Install ROS packages. There are two choices. **Please select ONLY ONE.** Then type "y" to continue the installation.
    1. The first choice installs all the features of the ROS. It takes about **1~2 hours**.
        ```bash
        sudo apt install ros-melodic-desktop-full
        ```
    2. The second choice does not include all the features of the ROS. The installation takes **30~40 minutes**, but you may need to install additional packages later.
        ```bash
        sudo apt install ros-melodic-desktop
        ```

5. Initialize rosdep. Once the ROS is installed, it must be initialized.

    ```bash
    sudo rosdep init
    ```

6. After initializing, update rosdep. rosdep enables you to easily install system dependencies for source you want to compile and is required to run some core components in ROS.

    ```bash
    rosdep update
    ```

    ![Install ros](img/install_ros2.PNG)

7. Environment setup

    ```bash
    echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
    source ~/.bashrc
    ```

8. Dependencies for building packages (It takes about 2 minutes.)

    ```bash
    sudo apt install python-rosinstall python-rosinstall-generator python-wstool build-essential
    ```

9. Verifying your installation

    ```bash
    printenv | grep ROS
    ```
    ![check ros](img/install_ros3.PNG)

---
[>> Click to go to the parent page.](/README.md)