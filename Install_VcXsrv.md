# VcXsrv

1. [Installation](#installation)
2. [How to use VcXsrv](#how-to-use-vcxsrv)

## Installation

This program is for Windows, so use it on Windows.

1. Download from [**here (sourceforge link)**](https://sourceforge.net/projects/vcxsrv/).

    ![VcXsrv Installation](img/VcXsrv1.PNG)

    ![VcXsrv Installation](img/VcXsrv2.PNG)

    ![VcXsrv Installation](img/VcXsrv3.PNG)

    **But now VcXsrv and Ubuntu are NOT connected.**

2. Open **WSL Linux** and copy and paste the following:

    ```bash
    echo "export DISPLAY=:0" >> ~/.bashrc
    source ~/.bashrc
    ```

## How to use VcXsrv

Running VcXsrv `only once` is enough. There is no reason to run the program multiple times.

![VcXsrv](img/VcXsrv4.PNG)

![VcXsrv](img/VcXsrv5.PNG)

![VcXsrv](img/VcXsrv6.PNG)

![VcXsrv](img/VcXsrv7.PNG)

![VcXsrv](img/VcXsrv8.PNG)

---
[>> Click to go to the parent page.](/README.md)