# WSL Installation

1. [What are things like WSL, Linux, and so on?](#what-are-things-like-wsl-linux-and-so-on)
2. [Activate WSL](#activate-wsl)
3. [Install Ubuntu](#install-ubuntu)

## What are things like WSL, Linux, and so on?

In a nutshell, [**Linux**](https://en.wikipedia.org/wiki/Linux) is a computer operating system such as Windows or MacOS. It is free and used by many engineers and programmers around the world.

- [**Ubuntu**](https://en.wikipedia.org/wiki/Ubuntu) is the name of the most famous series of the many Linux family.
- Linux series can be classified according to their history and derivation, Ubuntu is classified as [**Debian**](https://en.wikipedia.org/wiki/Debian) series.
- [**WSL**](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux) is a feature that makes it easy to use these Linux systems in our windows.

## Activate WSL

1. Open the **`Programs and Features`**(or **`프로그램 및 기능`**) from the **`Control Panel`**(or **`제어판`**).

    ![Control Panel](img/control_panel1.PNG)

2. Click the **`Turn Windows freatures on or off`**(or **`Windows 기능 켜기/끄기`**).
3. Enable **`Windows Subsystem for Linux`**(or **`Linux용 Windows 하위 시스템`**) and press the OK button. Then you are ready to install WSL.
   > If you can not find the WSL checkbox, update Windows to make it up to date and try again. WSL is also available in the Windows Home edition. If you don't know how to update Windows, click [**here**](https://support.microsoft.com/en-us/help/4027667/windows-10-update).

    ![Control Panel](img/control_panel2.PNG)
    ![Control Panel](img/control_panel3.PNG)
    ![Control Panel](img/control_panel4.PNG)

    > Note: You must restart to complete the update.

## Install Ubuntu

1. Open the Microsoft Store.

    ![Windows App Store](img/wsl1.PNG)

2. Install [**Ubuntu**](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot%3Aoverviewtab). You can easily find it by searching for **Ubuntu**.
    > **(Caution)** Do not install "Ubuntu 16.04"

    ![wsl](img/wsl2.PNG)

3. Now, you can find **Ubuntu** on the Start menu.

    ![wsl](img/wsl3.PNG)

4. After a while, you will see the window below.

    ![wsl](img/wsl5.PNG)

5. Enter the **username** you want to use and press `Enter`. User name can be simple. (For example, my user name is 'hr'.) Next, you will set a **password**.
   > Do not worry about what you can not see when you enter your password. On Linux terminals, you can not see the password you entered.

    ![wsl](img/wsl6.PNG)

6. Now Ubuntu is installed. Please enter the command below. (You may enter the password or the letter 'y'.)

    ```bash
    sudo apt-get update
    sudo apt-get upgrade
    ```
    > This process takes about 5 minutes.

---
[>> Click to go to the parent page.](/README.md)