# V-REP Installation

<!-- On **Windows** and **Linux**, vrep has different library files, and we need both. `Therefore, we must install vrep on both sides.` -->

## Windows

1. Go to the [official v-rep download page (click)](http://www.coppeliarobotics.com/downloads.html), and download the Windows version of v-rep edu.

    ![vrep official web](img/vrep1-win.PNG)

2. The next step is a general program installation of Windows, so I will skip further details.

## WSL

This step is omitted because it is not currently required.

<!-- 1. Open your web browser in the WSL.
2. Go to the download page.

    ![vrep official web](img/vrep1-linux1.PNG)

    ![vrep official web](img/vrep1-linux2.PNG)

    ![vrep official web](img/vrep1-linux3.PNG)

3. Close the browser and enter the following command to move to the download folder. (`cd` is an abbreviation for 'change directory'.)

    ```bash
    cd ~/Downloads
    ```

4. Unzip the downloaded file. As of January 2019, the version of vrep is 3.5.0, but your version may be different. (Tip: Type `tar -xzf V` and press `Tab` to use the autocomplete function!)

    ```bash
    tar –xzf V-REP_PRO_EDU_V3_5_0_Linux.tar.gz
    ```

    (In the picture below, `ls` is a command that lists computer files.)

    ![vrep dir](img/vrep3.PNG)

5. Update bash to use vrep comfortably. Do not forget to change `V3_5_0` to your version.

    ```bash
    echo "alias vrep='~/Downloads/V-REP_PRO_EDU_V3_5_0_Linux/vrep.sh'" >> ~/.bashrc
    source ~/.bashrc
    ```

6. From now on, if you want to run vrep, you can type:

    ```bash
    vrep
    ```

    ![vrep dir](img/vrep4.PNG) -->

---
[>> Click to go to the parent page.](/README.md)