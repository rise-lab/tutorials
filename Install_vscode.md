<!-- # VS Code Installation

This guide describes how to install vscode for WSL and Windows. **If vscode is installed in a window, it can also be used in WSL.**

1. Go to the download page.

    ![vscode official web](img/vscode1.PNG)

    ![vscode official web](img/vscode2.PNG)

2. Install

    ```bash
    sudo dpkg -i <file-name>
    ```

3. You can run vscode with the following command:

    ```bash
    code
    ```

    ![vscode](img/vscode4.PNG)

---
[>> Click to go to the parent page.](/README.md) -->