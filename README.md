# RISE Tutorial

We will use the [V-REP](http://www.coppeliarobotics.com/) simulator in the [ROS](http://www.ros.org/) environment. For a clean environment, install the following items in order.

1. [Installation](#installation)
   1. [WSL (Windows Subsystem for Linux)](#wsl-windows-subsystem-for-linux)
   2. [ROS (Robot Operating System)](#ros-robot-operating-system)
   3. [VcXsrv (VC X-server)](#vcxsrv-vc-x-server)
   4. [Web Browsers for WSL](#web-browsers-for-wsl)
   5. [V-REP](#v-rep)
   6. [Spyder (Python IDE)](#spyder-python-ide)
2. [Four-bar Linkage Example](#four-bar-linkage-example)

---

## Installation

### WSL (Windows Subsystem for Linux)

ROS is not recommended for use on Windows. Therefore, to use ROS on Windows, you need Windows Subsystem for Linux (WSL).
**If you are using native Linux, ignore this step.** WSL is just an alternative to Linux.

1. [WSL Installation Guide](/Install_WSL.md)

### ROS (Robot Operating System)

1. To install the ROS packages, see this [ROS Installation Guide](/Install_ROS.md)
2. Create a ROS Workspace.
   1. Create your [catkin workspace](http://wiki.ros.org/catkin/workspaces):

        ```bash
        mkdir -p ~/catkin_ws/src
        ```

   2. Build your catkin workspace:

        ```bash
        cd ~/catkin_ws/
        catkin_make
        ```

   3. Update your [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)):

        ```bash
        echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
        source ~/.bashrc
        ```

### VcXsrv (VC X-server)

>If you are using native Linux, ignore this step.

You can not see the [GUI](https://en.wikipedia.org/wiki/Graphical_user_interface) window when only WSL is installed. However, if you install VcXsrv, you can open various WSL programs using the GUI. VcXsrv is a Windows X-server based on the xorg git sources (like xming or cygwin's xwin), but compiled with Visual Studio.

1. [VcXsrv Installation Guide](/Install_VcXsrv.md)

### Web Browsers for WSL

To install V-REP, you must be able to access the web page and receive files.

1. Choose one of them.

    1. Firefox (**Recommended**)

        ```bash
        sudo apt install firefox
        ```

    2. Chromium : It has been reported to fail in some cases. [(link)](https://github.com/jessfraz/dockerfiles/issues/65)

        ```bash
        sudo apt install chromium-browser
        ```

2. You can open your browser with following commands.
   `VcXsrv must be running before executing browser.` If you do not remember how to use VcXsrv, check [**this post**](/Install_VcXsrv.md) again.

    1. Firefox

        ```bash
        firefox
        ```
        ![chromium](img/firefox.PNG)

    2. Chromium

        ```bash
        chromium-browser
        ```

        If an error occurs, you can avoid the error by adding the following options: --no-sandbox

        ```bash
        chromium-browser --no-sandbox
        ```

        ![chromium](img/chromium.PNG)

### V-REP

V-REP is a great robot simulator to get you started.

1. [V-REP Installation Guide](/Install_vrep.md)

### Spyder (Python IDE)

[Spyder](https://www.spyder-ide.org/) is an IDE for python with GUI similar to MATLAB.

```bash
sudo apt install spyder
```

Execute with the following command.
`VcXsrv must be running before executing Spyder.`

```bash
spyder
```

![spyder](img/spyder.PNG)

## Four-bar Linkage Example

[>> Go to example project](https://gitlab.com/rise-lab/four_bar_example)
